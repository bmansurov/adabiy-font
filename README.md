# adabiy-font
Font based on Literaturnaya with support for Uzbek letters

`sfd` files can be opened with [FontForge] (https://github.com/fontforge/fontforge).

TODO
====
Ў, Қ, Ғ, and Ҳ don't quite look exactly right. Find texts written in
Uzbek with this font and fix the mismatches.
